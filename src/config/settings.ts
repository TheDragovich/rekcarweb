export const settings = {
  isProd: process.env.NODE_ENV === 'production',
  baseUrl: process.env.REACT_APP_BASE_URL,
};
