import {createSlice, PayloadAction} from '@reduxjs/toolkit'
import {TState} from '../index'

type TSideBarState = { page: number, open: boolean }

const initialState: TSideBarState = {
  page: 1,
  open: true
}

const sideBarSlice = createSlice({
  name: 'sidebar',
  initialState,
  reducers: {
    toggleOpen(state, action: PayloadAction<boolean>) {
      state.open = action.payload
    },
    changePage(state, action: PayloadAction<number>) {
      state.page = action.payload
    }
  }
})

export const {toggleOpen, changePage} = sideBarSlice.actions
export const getSideBar = (state: TState) => state.sideBar
export default sideBarSlice.reducer
