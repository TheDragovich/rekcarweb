import {createSlice, PayloadAction} from '@reduxjs/toolkit'
import {TState} from '../index'

type TListLineProps = {
  id: number,
  status: string,
  name: string
  city: string
  description: string
  type: string
  period: string
  budget: string
}

type TAdCompanyState = {
  [id: number]: TListLineProps
}

const initialState: TAdCompanyState = {
  1: {
    id: 1,
    status: 'green',
    name: 'Фирма №1',
    city: 'Санкт-Петербург',
    description: 'Название рекламной кампании',
    type: 'Премиум',
    period: '01.11.2020 - 01.12.2020',
    budget: '2 000 000'
  }
}

const adCompanySlice = createSlice({
  name: 'adCompany',
  initialState,
  reducers: {
    addCompany(state, action: PayloadAction<string>) {
      //
    },
    removeCompany(state) {
      //
    }
  }
})

export const {addCompany, removeCompany} = adCompanySlice.actions
export const getAdCompany = (state: TState) => state.adCompany
export default adCompanySlice.reducer
