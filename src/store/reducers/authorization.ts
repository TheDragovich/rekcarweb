import {createSlice, PayloadAction} from '@reduxjs/toolkit'
import {TState} from '../index'

type TAuthorizationState = { token: string | null, isAuthenticated: boolean }

const initialState: TAuthorizationState = {
  token: null,
  isAuthenticated: false
}

const authorizationSlice = createSlice({
  name: 'authorization',
  initialState,
  reducers: {
    saveToken(state, action: PayloadAction<string>) {
      state.token = action.payload
      state.isAuthenticated = true
    },
    removeToken(state) {
      state.token = null
      state.isAuthenticated = false
    }
  }
})

export const {saveToken, removeToken} = authorizationSlice.actions
export const getAuthorization = (state: TState) => state.authorization
export const getAuthorizationStatus = (state: TState) => state.authorization.isAuthenticated
export default authorizationSlice.reducer
