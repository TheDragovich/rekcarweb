import {configureStore, getDefaultMiddleware} from '@reduxjs/toolkit'
import * as reducers from 'store/reducers'
import storage from 'redux-persist/lib/storage'
import {
  persistStore,
  persistReducer,
  FLUSH,
  REHYDRATE,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER
} from 'redux-persist'
import {combineReducers} from 'redux'

const persistConfig = {
  key: 'root',
  storage,
}

const rootReducer = combineReducers({
  ...reducers
})

export type TState = ReturnType<typeof rootReducer>

const persistedReducer = persistReducer(persistConfig, rootReducer)

const store = configureStore({
  reducer: persistedReducer,
  middleware: getDefaultMiddleware({
    serializableCheck: {
      ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER]
    }
  })
})

const persistor = persistStore(store)

export {store, persistor}

