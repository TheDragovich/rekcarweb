import {ReactComponent as Logo} from 'assets/images/Logo.svg'
import {ReactComponent as OrgName} from 'assets/images/OrgName.svg'
import {ReactComponent as Ellipse} from 'assets/images/Ellipse.svg'
import {ReactComponent as AdCompany} from 'assets/images/AdCompany.svg'
import {ReactComponent as Advertisers} from 'assets/images/Advertisers.svg'
import {ReactComponent as Drivers} from 'assets/images/Drivers.svg'
import {ReactComponent as AdCompanyLeftStatus} from 'assets/images/AdCompanyLeftStatus.svg'
import {ReactComponent as Exit} from 'assets/images/Exit.svg'
import {ReactComponent as InPending} from 'assets/images/InPending.svg'
import {ReactComponent as InPendingDetail} from 'assets/images/InPendingDetail.svg'
import {ReactComponent as InReject} from 'assets/images/InReject.svg'
import {ReactComponent as InRejectDetail} from 'assets/images/InRejectDetail.svg'
import {ReactComponent as InWork} from 'assets/images/InWork.svg'
import {ReactComponent as InWorkDetail} from 'assets/images/InWorkDetail.svg'
import {ReactComponent as OnHoliday} from 'assets/images/OnHoliday.svg'
import {ReactComponent as OnHolidayDetail} from 'assets/images/OnHolidayDetail.svg'
import {ReactComponent as Back} from 'assets/images/Back.svg'
import {ReactComponent as Filter} from 'assets/images/Filter.svg'
import {ReactComponent as FilterDetail} from 'assets/images/FilterDetail.svg'
import Car from 'assets/images/Car.png'

export {
  Logo,
  OrgName,
  Ellipse,
  AdCompany,
  Advertisers,
  Drivers,
  AdCompanyLeftStatus,
  Exit,
  InPending,
  InPendingDetail,
  InReject,
  InRejectDetail,
  InWork,
  InWorkDetail,
  OnHoliday,
  OnHolidayDetail,
  Back,
  Filter,
  FilterDetail,
  Car
}
