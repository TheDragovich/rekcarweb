import React from 'react'
import {store, persistor} from 'store'
import {Provider} from 'react-redux'
import {PersistGate} from 'redux-persist/integration/react'
import {MainRouter} from './router/Routes'
import {CssBaseline} from '@material-ui/core'
import './index.css'
import {DetailAdCompany} from "./page/adCompany/components/detailAdCompany"
import {AutoList} from "./page/adCompany/components/autoList";

const App = () => (
  <div>
    <header>
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <CssBaseline/>
          <DetailAdCompany/>
        </PersistGate>
      </Provider>
    </header>
  </div>
)

export default App
