import React, {FC, useCallback} from 'react'
import clsx from 'clsx'
import {
  Logo,
  OrgName,
  Ellipse,
  AdCompany,
  Drivers,
  Advertisers,
  Exit
} from 'assets/images'
import {toggleOpen, changePage, getSideBar} from 'store/reducers/sideBar'
import {removeToken} from 'store/reducers/authorization'
import {useDispatch, useSelector} from 'react-redux'
import {makeStyles, useTheme} from '@material-ui/core/styles'
import {
  AppBar,
  Button,
  Container,
  Drawer,
  IconButton,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Toolbar,
  Typography
} from '@material-ui/core'

const drawerWidth = 295
const MENU_LIST = [
  {id: 1, title: 'Рекламные кампании', Icon: AdCompany},
  {id: 2, title: 'Водители', Icon: Drivers},
  {id: 3, title: 'Рекламодатели', Icon: Advertisers},
]

type IProps = {}

export const MainPage: FC<IProps> = ({children}) => {
  const classes = useStyles()
  const theme = useTheme()
  const {open, page} = useSelector(getSideBar)
  const dispatch = useDispatch()

  const handleDrawer = useCallback(() => dispatch(toggleOpen(!open)), [dispatch, open])
  const handleLogOut = useCallback(() => dispatch(removeToken()), [dispatch])
  const handlePage = useCallback((page: number) => () => dispatch(changePage(page)), [dispatch])

  return (
    <div className={classes.root}>
      <Drawer
        variant='permanent'
        anchor="left"
        className={clsx(classes.drawer, {
          [classes.drawerOpen]: open,
          [classes.drawerClose]: !open,
        })}
        classes={{
          paper: clsx({
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open,
          }),
        }}
      >
        <div className={classes.listMenu}>
          <div className={classes.listMenuTop}>
            <div className={classes.paper}>
              <div className={classes.sideBar}>
                <div>
                  <Logo className={classes.logoSVG}/>
                </div>
                <Typography component='h1' variant='h5'>
                  <OrgName className={open ? classes.orgNameOpen : classes.orgNameClose}/>
                </Typography>
              </div>
            </div>
            <List>
              {MENU_LIST.map(({id, title, Icon}) => (
                <Button onClick={handlePage(id)} key={id} className={classes.pageButton}>
                  <ListItem className={classes.item} button>
                    <ListItemIcon className={classes.itemIcon}>
                      <Icon fill={id === page ? '#CC2631' : '#000000'}/>
                    </ListItemIcon>
                    {open ? <ListItemText className={classes.itemText} primary={title}/> : null}
                  </ListItem>
                </Button>
              ))}
            </List>
          </div>
          <div className={classes.listMenuBottom}>
            <>
              <List className={classes.exitList}>
                <Button onClick={handleLogOut} key={'exit'} className={classes.pageButton}>
                  <ListItem className={classes.item} button>
                    <ListItemIcon className={classes.itemIcon}>
                      <Exit className={classes.exit}/>
                    </ListItemIcon>
                    {open ? <ListItemText className={classes.itemText} primary={'Выйти'}/> : null}
                  </ListItem>
                </Button>
              </List>
            </>
            <>
              <Toolbar className={classes.toolbar}>
                <IconButton onClick={handleDrawer}>
                  <Ellipse/>
                </IconButton>
              </Toolbar>
            </>
          </div>
        </div>
      </Drawer>
      <main className={clsx(classes.content, {
        [classes.contentShift]: open,
      })}
      >
        <div className={classes.drawerHeader}/>
        {children}
      </main>
    </div>
  )
}

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'none',
  },
  sideBar: {
    height: '160px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexWrap: 'wrap',
    flexDirection: 'row',
  },
  logoSVG: {
    height: '48.76px',
    width: '39.01px',
    margin: '10px',
  },
  listMenu: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    height: '100%',
  },
  listMenuTop: {},
  listMenuBottom: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-end',
    height: '100%',
  },
  exitList: {
    height: '64px',
  },
  exit: {
    height: '16px',
    width: '21px',
  },
  orgNameOpen: {
    height: '32px',
    width: '136.37px',
    margin: '10px',
  },
  orgNameClose: {
    height: '19.24px',
    width: '82px',
    margin: '10px',
  },
  body: {
    display: 'flex',
  },
  root: {
    display: 'flex',
  },
  appBar: {
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    overflow: 'hidden',
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(2),
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  contentShift: {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: 0,
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
    overflow: 'hidden',
  },
  drawerClose: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: '105px',
  },
  toolbar: {
    display: 'flex',
    padding: theme.spacing(0, 1),
    ...theme.mixins.toolbar,
    width: '100%',
    paddingLeft: '32px',
    paddingBottom: '8px',
  },
  pageButton: {
    width: '100%',
  },
  item: {
    height: '60px'
  },
  itemIcon: {
    justifyContent: 'center'
  },
  itemText: {
    width: '160px',
    paddingLeft: '24px',
  },
}))
