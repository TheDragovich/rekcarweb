import React, {FC, useCallback} from 'react'
import {ReactComponent as Logo} from 'assets/images/Logo.svg'
import {ReactComponent as OrgName} from 'assets/images/OrgName.svg'
import {
  Button,
  Container,
  TextField,
  Typography,
} from '@material-ui/core'
import {makeStyles} from '@material-ui/core/styles'
import {useDispatch} from 'react-redux'
import {saveToken} from 'store/reducers/authorization'
import {useForm} from 'react-hook-form'

interface IFormInputs {
  email: string
  password: string
}

type IProps = {}

export const Authorization: FC<IProps> = () => {
  const classes = useStyles()
  const dispatch = useDispatch()
  const {register, handleSubmit, errors} = useForm<IFormInputs>({
    mode: 'onSubmit',
    reValidateMode: 'onChange',
    resolver: undefined,
    context: undefined,
    criteriaMode: 'firstError',
    shouldFocusError: true,
    shouldUnregister: true,
    defaultValues: {
      email: 'asd@asd.asd',
      password: 'asd'
    },
  })

  const auth = useCallback(() => dispatch(saveToken('asdasd')), [dispatch])
  const onSubmit = useCallback((data: IFormInputs, errors: any) => {
    auth()
  }, [useForm])

  return (
    <Container component='main' maxWidth='xs'>
      <div className={classes.paper}>
        <div className={classes.logo}>
          <Logo/>
        </div>
        <Typography component='h1' variant='h5'>
          <OrgName/>
        </Typography>
        <form className={classes.form} onSubmit={handleSubmit(onSubmit)}>
          <div className={classes.textUnderField}>
            E-mail
          </div>
          <TextField
            variant='outlined'
            margin='normal'
            required
            fullWidth
            size='small'
            id='email'
            name='email'
            autoComplete='email'
            autoFocus
            className={classes.textInput}
            inputRef={register({
              pattern: {
                value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                message: 'Введите e-mail'
              },
              required: true
            })}
          />
          <div className={classes.error}>
            {errors?.email?.message}
          </div>
          <div className={classes.textUnderField}>
            Пароль
          </div>
          <TextField
            variant='outlined'
            margin='normal'
            required
            fullWidth
            size='small'
            name='password'
            type='password'
            id='password'
            autoComplete='current-password'
            className={classes.textInput}
            inputRef={register({
              minLength: {
                value: 1,
                message: 'Введите пароль'
              },
              required: true
            })}
          />
          <div className={classes.error}>
            {errors?.password?.message}
          </div>
          <Button
            type='submit'
            fullWidth
            variant='contained'
            size='small'
            color='primary'
            classes={{containedPrimary: classes.containedPrimary, root: classes.buttonText}}
            className={classes.submit}
          >
            Вход
          </Button>
        </form>
      </div>
    </Container>
  )
}

const useStyles = makeStyles((theme) => ({
  logo: {
    marginBottom: theme.spacing(3),
  },
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(12),
  },
  submit: {
    marginTop: theme.spacing(2),
  },
  containedPrimary: {
    backgroundColor: '#2D9CDB',
    '&:hover': {
      backgroundColor: '#2D9CDB',
    },
  },
  textInput: {
    '& .MuiOutlinedInput-root': {
      height: '32px',
      '&.Mui-focused fieldset': {
        borderColor: '#2D9CDB',
      },
    },
    marginTop: 0,
    marginBottom: theme.spacing(2),
  },
  textUnderField: {
    color: '#7D7D7D',
    fontWidth: '300',
    fontSize: '16px',
    lineHeight: '18.5px',
    fontStyle: 'normal',
    fontFamily: 'Roboto, sans-serif',
    marginBottom: '2px',
  },
  buttonText: {
    textTransform: 'inherit',
    fontFamily: 'Roboto, sans-serif',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: '15px',
    lineHeight: '18px',
  },
  error: {
    color: 'red',
  },
}))
