import React, {FC} from 'react';
import {makeStyles} from '@material-ui/core/styles'
import {InWork, InPending, InReject, OnHoliday, Car} from "../../../assets/images";

type TListLineProps = {}

export const AutoList: FC<TListLineProps> = () => {
  const classes = useStyles()

  return (
    <div className={classes.paper}>
      <div className={classes.imgs}>
        <div className={classes.carPreview}>
          <img src={Car} className={classes.carPreviewImg} alt='car'/>
        </div>
        <div className={classes.carStatus}>
          <InWork/>
        </div>
      </div>
      <div className={classes.detailInfo}>
        <div className={classes.fieldDescription}>
          <div className={classes.fieldDescriptionName}>
            Марка:
          </div>
          <div className={classes.fieldDescriptionValue}>
            Dodge
          </div>
        </div>
        <div className={classes.fieldDescription}>
          <div className={classes.fieldDescriptionName}>
            Модель:
          </div>
          <div className={classes.fieldDescriptionValue}>
            RAW2500
          </div>
        </div>
        <div className={classes.fieldDescription}>
          <div className={classes.fieldDescriptionName}>
            VIN-номер:
          </div>
          <div className={classes.fieldDescriptionValue}>
            WAUZZZ8AZMA123456
          </div>
        </div>
        <div className={classes.fieldDescription}>
          <div className={classes.fieldDescriptionName}>
            ДТП:
          </div>
          <div className={classes.fieldDescriptionValue}>
            Участвовал
          </div>
        </div>
        <div className={classes.fieldDescription}>
          <div className={classes.fieldDescriptionName}>
            Город:
          </div>
          <div className={classes.fieldDescriptionValue}>
            Санкт-Петербург
          </div>
        </div>
      </div>
    </div>
  )
}

const useStyles = makeStyles((theme) => ({
  paper: {
    background: '#FFFFFF',
    border: '1px solid #FFFFFF',
    boxSizing: 'border-box',
    borderRadius: '8px',
    display: 'flex',
    flexDirection: 'row',
    padding: theme.spacing(2),
    width: '548px',
    height: '166px',
  },
  imgs: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    height: '134px',
    alignItems: 'center',
    marginRight: '14px',
  },
  carPreview: {},
  carPreviewImg: {
    height: '80px',
    wight: '80px',
  },
  carStatus: {},
  detailInfo: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    height: '134px',
  },
  fieldDescription: {
    display: 'flex',
    flexDirection: 'row'
  },
  fieldDescriptionName: {
    width: '86px',
    display: 'flex',
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 300,
    fontSize: '16px',
    lineHeight: '19px',
    textAlign: 'right',
    color: '#7D7D7D',
  },
  fieldDescriptionValue: {
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 500,
    fontSize: '18px',
    lineHeight: '21px',
    color: '#000000',
  }
}))
