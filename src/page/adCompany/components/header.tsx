import React, {FC} from 'react'
import {makeStyles} from "@material-ui/core/styles";
import {FormControl, InputLabel, MenuItem, Select, TextField} from "@material-ui/core";

type TProps = {}

export const Header: FC<TProps> = () => {
  const classes = useStyles()

  return (
    <div className={classes.header}>
      <div className={classes.leftRow}>
        Рекламные кампании
      </div>
      <div className={classes.rightRow}>
        <form className={classes.root} noValidate autoComplete="off">
          <TextField id="outlined-basic" variant="outlined">asd</TextField>
          <FormControl variant="outlined" className={classes.formControl}>
            <InputLabel id="demo-simple-select-outlined-label">
            </InputLabel>
            <Select
              labelId="demo-simple-select-outlined-label"
              id="demo-simple-select-outlined"
            >
              <MenuItem value="">
                <em>None</em>
              </MenuItem>
              <MenuItem value={10}>Ten</MenuItem>
              <MenuItem value={20}>Twenty</MenuItem>
              <MenuItem value={30}>Thirty</MenuItem>
            </Select>
          </FormControl>
        </form>
      </div>
    </div>
  )
}

const useStyles = makeStyles((theme) => ({
  header: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '100%',
  },
  leftRow: {
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontSize: '24px',
    lineHeight: '28px',
    color: '#7D7D7D',
  },
  root: {},
  rightRow: {},
  formControl: {},
}))
