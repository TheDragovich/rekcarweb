import React, {FC} from 'react';
import {makeStyles} from '@material-ui/core/styles'
import {Back} from "../../../assets/images";
import {AutoList} from "./autoList";

type TListLineProps = {}

export const DetailAdCompany: FC<TListLineProps> = () => {
  const classes = useStyles()

  return (
    <div className={classes.paper}>
      <div className={classes.header}>
        <Back className={classes.back}/>
        <div className={classes.headerText}>
          Название кампании
        </div>
      </div>
      <div className={classes.info}>
        <div className={classes.description}>
          <div className={classes.descriptionHeader}>
            Описание кампании
          </div>
          <div className={classes.descriptionBody}>
            <div className={classes.fieldDescription}>
              <div className={classes.fieldDescriptionName}>
                Организация
              </div>
              <div className={classes.fieldDescriptionValue}>
                Фирма №1
              </div>
            </div>
            <div className={classes.fieldDescription}>
              <div className={classes.fieldDescriptionName}>
                Город:
              </div>
              <div className={classes.fieldDescriptionValue}>
                Санкт-Петербург
              </div>
            </div>
            <div className={classes.fieldDescription}>
              <div className={classes.fieldDescriptionName}>
                Класс авто:
              </div>
              <div className={classes.fieldDescriptionValue}>
                Премиум
              </div>
            </div>
            <div className={classes.fieldDescription}>
              <div className={classes.fieldDescriptionName}>
                Тираж:
              </div>
              <div className={classes.fieldDescriptionValue}>
                500
              </div>
            </div>
            <div className={classes.fieldDescription}>
              <div className={classes.fieldDescriptionName}>
                Сроки проведения:
              </div>
              <div className={classes.fieldDescriptionValue}>
                01.11.2020 - 01.12.2020
              </div>
            </div>
            <div className={classes.fieldDescription}>
              <div className={classes.fieldDescriptionName}>
                Бюджет:
              </div>
              <div className={classes.fieldDescriptionValue}>
                2 000 000
              </div>
            </div>
            <div className={classes.fieldDescription}>
              <div className={classes.fieldDescriptionName}>
                E-mail
              </div>
              <div className={classes.fieldDescriptionValue}>
                firma1@mail.ru
              </div>
            </div>
            <div className={classes.descriptionText}>
              Совокупность мероприятий, которые проводятся с целью привлечь внимание целевой аудитории к товарам,
              продуктам, услугам производителя. Эффективность РК зависит от содержания коммерческого послания, времени
              проведения и множества других факторов.
            </div>

          </div>
        </div>
        <div className={classes.description}>
          <div className={classes.descriptionHeader}>
            Транспортные средства (4)
          </div>
          <div className={classes.descriptionBody}>
            <AutoList/>
          </div>
        </div>
        <div className={classes.description}>
          <div className={classes.descriptionHeader}>
            Статистика
          </div>
          <div className={classes.descriptionBody}>
          </div>
        </div>
      </div>
    </div>
  )
}

const useStyles = makeStyles((theme) => ({
  paper: {
    border: '1px solid #F2F2F2',
    boxSizing: 'border-box',
    borderRadius: '8px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    width: '100%',
    height: 'auto'
    // margin: theme.spacing(1),
  },
  header: {
    display: 'flex',
    justifyContent: 'left',
    alignItems: 'center',
    width: '100%',
    height: '60px',
  },
  back: {
    marginLeft: '17px',
  },
  headerText: {
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontSize: '24px',
    lineHeight: '28px',
    color: '#7D7D7D',
    marginLeft: '11px',
    fontWeight: 300,
  },
  info: {
    display: 'flex',
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-between',
    borderTop: '1px solid #F2F2F2',
    borderBottom: '1px solid #F2F2F2',
    boxSizing: 'border-box',
  },
  description: {
    display: 'flex',
    flexGrow: 1,
    flexDirection: 'column',
    borderLeft: '1px solid #F2F2F2',
    borderRight: '1px solid #F2F2F2',
    boxSizing: 'border-box',
  },
  descriptionHeader: {
    display: 'flex',
    alignItems: 'center',
    height: '53px',
    padding: theme.spacing(3, 0, 3, 3),
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontSize: '18px',
    lineHeight: '21px',
    borderBottom: '1px solid #F2F2F2',
    boxSizing: 'border-box',
    color: '#000000',
  },
  descriptionBody: {
    padding: theme.spacing(3),
  },
  fieldDescription: {
    display: 'flex',
    flexDirection: 'column',
    marginBottom: theme.spacing(2),
  },
  fieldDescriptionName: {
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 300,
    fontSize: '16px',
    lineHeight: '19px',
    marginBottom: '2px',
    color: '#7D7D7D',
  },
  fieldDescriptionValue: {
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 500,
    fontSize: '18px',
    lineHeight: '21px',
    color: '#000000',
  },
  descriptionText: {
    border: ' 1px solid #F2F2F2',
    boxSizing: 'border-box',
    borderRadius: '8px',
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 300,
    fontSize: '16px',
    lineHeight: '19px',
    color: '#000000',
    display: 'flex',
    flexWrap: 'wrap',
  },
  carsBody: {},
  gpsBody: {},
  text: {
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: '15px',
    lineHeight: '18px',
    display: 'flex',
    alignItems: 'center',
  }
}))
