import React, {FC} from 'react';
import {makeStyles} from '@material-ui/core/styles'
import {AdCompanyLeftStatus} from "../../../assets/images"
import clsx from 'clsx'

// TODO: export from redux
export type TListLineProps = {
  id: number,
  status: string,
  name: string,
  city: string,
  description: string,
  type: string,
  period: string,
  budget: string,
}

export const ListLine: FC<TListLineProps> = ({status, name, budget,city,description, period, type}) => {
  const classes = useStyles()

  return (
      <div className={classes.paper}>
        <div className={classes.image}>
          <AdCompanyLeftStatus fill={status}/>
        </div>
        <div className={clsx(classes.name, classes.text)}>
          {name}
        </div>
        <div className={clsx(classes.city, classes.text)}>
          {city}
        </div>
        <div className={clsx(classes.description, classes.text)}>
          {description}
        </div>
        <div className={clsx(classes.firmType, classes.text)}>
          {type}
        </div>
        <div className={clsx(classes.period, classes.text)}>
          {period}
        </div>
        <div className={clsx(classes.budget, classes.text)}>
          {budget}
        </div>
      </div>
  )
}

const useStyles = makeStyles((theme) => ({
  paper: {
    border: '1px solid #F2F2F2',
    boxSizing: 'border-box',
    borderRadius: '8px',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    justifyContent: 'space-between',
    margin: theme.spacing(1),
  },
  image: {
    height: '46px',
    flexGrow: 1,
  },
  name: {
    flexGrow: 3,
  },
  city: {
    flexGrow: 15,
  },
  description: {
    flexGrow: 15,
  },
  firmType: {
    flexGrow: 3,
  },
  period: {
    flexGrow: 1,
  },
  budget: {
    flexGrow: 1,
  },
  text: {
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: '15px',
    lineHeight: '18px',
    display: 'flex',
    alignItems: 'center',
  }
}))
