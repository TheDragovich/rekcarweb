import React, {FC, useEffect} from 'react'
import {ListLine, TListLineProps} from './components/listLine'
import {Header} from './components/header'
import {makeStyles} from '@material-ui/core/styles'
import {useDispatch, useSelector} from 'react-redux'
import {getAdCompany} from '../../store/reducers/adCompany'

type TProps = {}

export const AdCompany: FC<TProps> = () => {
  const classes = useStyles()

  const someData: TListLineProps = {
    id: 1,
    status: 'green',
    name: 'Фирма №1',
    city: 'Санкт-Петербург',
    description: 'Название рекламной кампании',
    type: 'Премиум',
    period: '01.11.2020 - 01.12.2020',
    budget: '2 000 000',
  }
// useEffect for adcompany

  const dispatch = useDispatch()
  const list = useSelector(getAdCompany)
  console.log(list)

  return (
    <div className={classes.adcompany}>
      <div className={classes.header}>
        <Header/>
      </div>
      <div className={classes.listElem}>
        {Object.keys(list).map((key, item) => <ListLine key={parseInt(key)+Math.floor(Math.random() * Math.floor(100))} {...list[parseInt(key)]}/>)}
      </div>
    </div>
  )
}

const useStyles = makeStyles((theme) => ({
  adcompany: {
    height: 'auto',
    width: '100%',
    padding: 0
  },
  header: {
    width: '100%',
  },
  listElem: {
    width: '100%',
  },
  paper: {
    border: '1px solid #F2F2F2',
    boxSizing: 'border-box',
    borderRadius: '8px',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    justifyContent: 'space-between',
    margin: theme.spacing(1),
  },
}))
