import React from 'react'
import {
  Route,
  Switch,
  Redirect,
  Router,
} from 'react-router'
import {MainPage} from '../page/sideBar'
import {createBrowserHistory} from 'history'
import {useSelector} from 'react-redux'
import {getAuthorizationStatus} from '../store/reducers/authorization'
import {Authorization} from 'page/authorization'
import {AdCompany} from "../page/adCompany/adCompany";

const customHistory = createBrowserHistory()

export const MainRouter = () => {
  const isAuthenticated = useSelector(getAuthorizationStatus)

  if (isAuthenticated) {
    return (
      <MainPage>
        <Router history={customHistory}>
          <Switch>
            <Route path='/ad-company' exact component={AdCompany}/>
            <Redirect to='/ad-company'/>
          </Switch>
        </Router>
      </MainPage>
    )
  }

  return (
    <Router history={customHistory}>
      <Switch>
        <Route path='/' exact component={Authorization}/>
        <Redirect to='/'/>
      </Switch>
    </Router>
  )
}
